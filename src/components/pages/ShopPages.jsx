import { gql, useMutation, useQuery } from '@apollo/client';
import { Button, Typography } from '@material-tailwind/react';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

const GET_HISTORY_TRANSAKSI_CUSTOMER = gql`
  query GetHistoryTransaksiCustomer($qrCode: String) {
    historyTransaksiCustomer(qrCode: $qrCode) {
      _id
      qr_code
      rfid
      harga_satuan
      jumlah
      date
    }
  }
`;

const EDIT_JUMLAH_TRANSAKSI = gql`
  mutation EditJumlahTransaksi($id: ID!, $jumlah: Int!) {
    editJumlahTransaksi(_id: $id, jumlah: $jumlah) {
      _id
      qr_code
      rfid
      harga_satuan
      jumlah
      date
    }
  }
`;

export default function ShopPages() {
  const location = useLocation();
  const navigate = useNavigate();
  const [customerData, setCustomerData] = useState(null);
  const [deletedSuccess, setDeletedSuccess] = useState(false); // State untuk menandakan berhasil menghapus transaksi

  const [editJumlahTransaksi] = useMutation(EDIT_JUMLAH_TRANSAKSI);
  const handleTambahJumlah = async (id, currentJumlah) => {
    try {
      await editJumlahTransaksi({
        variables: { id: id, jumlah: parseInt(currentJumlah, 10) + 1 },
      });
      window.location.reload();
    } catch (error) {
      console.error('Error updating jumlah transaksi:', error);
    }
  };

  const handleKurangiJumlah = async (id, currentJumlah) => {
    try {
      await editJumlahTransaksi({
        variables: { id: id, jumlah: parseInt(currentJumlah, 10) - 1 },
      });

      window.location.reload();
    } catch (error) {
      console.error('Error updating jumlah transaksi:', error);
    }
  };

  const handleSimpanTransaksi = async () => {
    try {
      await axios.get('http://localhost:3333/transfer-data');
      navigate('/riwayat-transaksi-customer');
      alert('Simpan Berhasil');
    } catch (error) {
      console.error('Error saving transaction:', error);
    }
  };

  const handleMainPages = () => {
    navigate('/');
    // Tidak perlu mengubah customerData karena logout tidak memerlukan data customer
  };

  const transaksiPages = () => {
    navigate('/shop', { state: { customerData: customerData } });
  };

  const historyTransaksiPages = () => {
    navigate('/riwayat-transaksi-customer', {
      state: { customerData: customerData },
    });
  };

  useEffect(() => {
    // Periksa apakah ada data di dalam state location
    if (location.state && location.state.customerData) {
      setCustomerData(location.state.customerData);
    }
  }, [location]);

  useEffect(() => {
    // Memuat ulang halaman jika transaksi berhasil dihapus
    if (deletedSuccess) {
      window.location.reload();
    }
  }, [deletedSuccess]);

  const handleDeleteTransaksi = async (transaksiId) => {
    try {
      const response = await fetch(
        `http://localhost:3333/delete-cart/${transaksiId}`,
        {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      if (response.ok) {
        // Set state deletedSuccess menjadi true
        setDeletedSuccess(true);
      } else {
        console.error('Failed to delete transaksi');
      }
    } catch (error) {
      console.error('Error deleting transaksi:', error);
    }
  };

  let qrCode = '';
  if (customerData && customerData.qrCode) {
    qrCode = customerData.qrCode;
  }
  const { loading, error, data } = useQuery(GET_HISTORY_TRANSAKSI_CUSTOMER, {
    variables: { qrCode: qrCode },
  });

  if (loading)
    return (
      <div className='loading-spinner bg-[#3AAFA9] min-h-screen flex flex-wrap justify-center items-center'></div>
    );
  if (error) return <div className='error-message'>Error: {error.message}</div>;

  return (
    <div className='bg-[#3AAFA9] min-h-screen flex flex-col justify-center items-center'>
      <div className='absolute top-0 h-20 bg-white left-0 w-full flex justify-between bg-[#3AAFA9]'>
        <div className='flex items-center'>
          <Typography className='ml-4 font-bold text-black'>X-Mart</Typography>
          <Button
            onClick={transaksiPages}
            className='mx-2 font-semibold text-gray-100 bg-black'
          >
            Transaksi
          </Button>
          <Button
            onClick={historyTransaksiPages}
            className='mx-2 font-semibold text-gray-100 bg-black'
          >
            History Transaksi
          </Button>
        </div>
        <div className='flex items-center'>
          {customerData && (
            <div className='flex '>
              <p className='mx-2 text-black bg-transparent'>
                {customerData.nama}
              </p>
            </div>
          )}
          <Button
            onClick={handleMainPages}
            className='mx-2 font-semibold text-gray-100 bg-black'
          >
            Logout
          </Button>
        </div>
      </div>

      <div className='flex mt-[5rem] justify-center'>
        <div className='w-3/5 mr-2'>
          <div className='flex justify-center p-3 bg-white'>
            {customerData && (
              <div className='text-black'>
                <p>QRCode: {customerData.qrCode}</p>
                <p>Nama Customer: {customerData.nama}</p>
                <p>Wallet: Rp.{customerData.wallet}</p>
              </div>
            )}
          </div>
        </div>

        <div className='w-5/5'>
          <div className='flex justify-center p-3 bg-white'>
            {data.historyTransaksiCustomer.length === 0 ? (
              <div className='mb-5 text-lg font-semibold text-black empty-data-message'>
                Belum Ada Transaksi.
              </div>
            ) : (
              <div className='overflow-x-auto table-wrapper'>
                <Typography className='font-semibold'>Transaksi</Typography>

                <table className='min-w-full'>
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>RFID</th>
                      <th>Harga Satuan</th>
                      <th>Jumlah</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {data.historyTransaksiCustomer.map(
                      (historyTransaksiCustomer) => (
                        <tr
                          key={historyTransaksiCustomer._id}
                          className='bg-blue-gray-50'
                        >
                          <td>{historyTransaksiCustomer._id}</td>
                          <td>{historyTransaksiCustomer.rfid}</td>
                          <td>{historyTransaksiCustomer.harga_satuan}</td>
                          <td>{historyTransaksiCustomer.jumlah}</td>
                          <Button
                            onClick={() =>
                              handleKurangiJumlah(
                                historyTransaksiCustomer._id,
                                historyTransaksiCustomer.jumlah,
                              )
                            }
                            className='text-black bg-transparent'
                          >
                            -
                          </Button>
                          <Button
                            onClick={() =>
                              handleTambahJumlah(
                                historyTransaksiCustomer._id,
                                historyTransaksiCustomer.jumlah,
                              )
                            }
                            className='text-black bg-transparent'
                          >
                            +
                          </Button>

                          <td>
                            <Button
                              onClick={() =>
                                handleDeleteTransaksi(
                                  historyTransaksiCustomer._id,
                                )
                              }
                              className='text-black bg-transparent'
                            >
                              Delete
                            </Button>
                          </td>
                        </tr>
                      ),
                    )}
                  </tbody>
                </table>
                <Button className='mt-4' onClick={handleSimpanTransaksi}>
                  Simpan Transaksi
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
